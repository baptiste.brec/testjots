import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingPage extends StatefulWidget{

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State <SettingPage>{

  String key = "username";
  String username="";
  bool Open=false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get();
  }



  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Settings Page"),
      ),
      drawer: Drawer(
        child: Container(
          color: Colors.white,
          child: DrawerHeader(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Card(
                  elevation: 10.0,
                  child: Container(
                    width: MediaQuery.of(context).size.width/2,
                    height: MediaQuery.of(context).size.width/2,
                    child: Image.asset("user.png",
                      fit: BoxFit.fitHeight,),
                  ),
                ),
                TextButton(
                    onPressed:(){

                    },
                    child: const Text("Edit",style: TextStyle(color: Colors.blue),)
                ),
                TextButton.icon(
                  icon: const Icon(Icons.manage_accounts),
                  style: TextButton.styleFrom(primary: Colors.black),
                  onPressed: (){

                  },
                  label: const Text("Account"),
                ),
                ElevatedButton.icon(
                  icon: const Icon(Icons.settings),
                  style: ElevatedButton.styleFrom(primary: Colors.black),
                  onPressed: (){

                  },
                  label: const Text("Settings"),
                ),
                TextButton.icon(
                  icon: const Icon(Icons.logout),
                  style: TextButton.styleFrom(primary: Colors.black),
                  onPressed: (){

                  },
                  label: const Text("Log out"),
                ),
              ],
            ),
          ),
        ),
      ),
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  const Text("Theme", style: TextStyle(fontSize: 20.0),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Switch(
                        value: Open,
                        onChanged: (b){
                          setState(() {
                            Open= !Open;
                          });
                        },
                        activeColor: Colors.black,
                      ),
                      Text((Open)? "Dark Mode" : "Light Mode")
                    ],
                  ),
                ],
              ),
              Column(
                children: [
                  const Text("Hotkeys", style: TextStyle(fontSize: 20.0)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      TextButton(
                          onPressed: (){

                          },
                          child: const Text("Open App",style: TextStyle(color: Colors.black))),
                      IconButton(
                        onPressed: () {  },
                        icon: const Icon(Icons.qr_code_scanner),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(backgroundColor: Colors.white),
                          onPressed: (){ },
                          child: const Text("J", style: TextStyle(color: Colors.black),)
                      ),
                      OutlinedButton(
                          onPressed: (){ },
                          child: const Text("Record new shrtcut",style: TextStyle(color: Colors.black)))
                    ],
                  ),
                ],
              ),
              Column(
                children: const [
                  Text("Other", style: TextStyle(color: Colors.black, fontSize: 20)),
                  Text("Request Permissions", style: TextStyle(color: Colors.black)),
                ],
              )
            ],
          ),
      ),
    );
  }

  void get() async{
    SharedPreferences sharedPreferences= await SharedPreferences.getInstance();
    String? name= await sharedPreferences.getString(key);
    if (name!=null){
      setState(() {
        username=name;
      });
    }
  }
}
