import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_jots/account_page.dart';
import 'package:test_jots/settings_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String key = "username";
  String username="";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext){
                    return AccountPage();
                  }));
                  //dialogAccount();
                },
                child: const Text("Go to the Account",
                  style: TextStyle(
                    color: Colors.black,
                    fontStyle: FontStyle.italic
                  ),
                )
            ),
            TextButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext){
                    return SettingPage();
                  }));
                },
                child: const Text("Go to the Settings",
                  style: TextStyle(
                      color: Colors.black,
                      fontStyle: FontStyle.italic
                  ),
                )
            ),
            Text(username)
          ],
        ),
      ),
    );
  }

  void get() async{
    SharedPreferences sharedPreferences= await SharedPreferences.getInstance();
    String? name= await sharedPreferences.getString(key);
    if (name!=null){
      setState(() {
        username=name;
      });
    }
  }
}
