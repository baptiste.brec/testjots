import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountPage extends StatefulWidget{

  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State <AccountPage>{

  String key = "username";
  String username="";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get();
  }



  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Account Page"),
      ),
      drawer: Drawer(
        child: Container(
          color: Colors.white,
          child: DrawerHeader(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Card(
                  elevation: 10.0,
                  child: Container(
                    width: MediaQuery.of(context).size.width/2,
                    height: MediaQuery.of(context).size.width/2,
                    child: Image.asset("user.png",
                      fit: BoxFit.fitHeight,),
                  ),
                ),
                TextButton(
                    onPressed:(){

                  },
                    child: const Text("Edit",style: TextStyle(color: Colors.blue),)
                  ),
                ElevatedButton.icon(
                  icon: const Icon(Icons.manage_accounts),
                  style: ElevatedButton.styleFrom(primary: Colors.black),
                    onPressed: (){

                    },
                    label: const Text("Account"),
                  ),
                TextButton.icon(
                  icon: const Icon(Icons.settings),
                  style: TextButton.styleFrom(primary: Colors.black),
                  onPressed: (){

                  },
                  label: const Text("Settings"),
                ),
                TextButton.icon(
                  icon: const Icon(Icons.logout),
                  style: TextButton.styleFrom(primary: Colors.black),
                  onPressed: (){

                  },
                  label: const Text("Log out"),
                ),
              ],
            ),
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("Name", style: TextStyle(color: Colors.grey,fontSize: 20),),
                Text(username),
                TextButton(
                    onPressed: (){
                      dialogAccount();
                },
                    child: const Text("Change Name", style: TextStyle(color: Colors.blue),))
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("Email", style: TextStyle(color: Colors.black,fontSize: 20),),
                Text("$username@jots.ai"),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("Password", style: TextStyle(color: Colors.black,fontSize: 20),),
                TextButton(
                    onPressed: (){

                    },
                    child: const Text("Change password", style: TextStyle(color: Colors.blue),))
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("Account", style: TextStyle(color: Colors.black,fontSize: 20),),
                TextButton(
                    onPressed: (){

                    },
                    child: const Text("Delete account", style: TextStyle(color: Colors.red),))
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<Null> dialogAccount() async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context){
        return SimpleDialog(
          title: const Text("Change your name", textScaleFactor: 1.4,),
          contentPadding: const EdgeInsets.all(10.0),
          children: <Widget>[
            Container(height: 20.0,),
            TextField(
              textAlign: TextAlign.center,
              onSubmitted: (String str){
                add(str);
              },
              decoration: const InputDecoration(
                  labelText:"Write your username here"
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(primary :Colors.teal),
              onPressed: (){
                Navigator.pop(context);
              },
              child: const Text("Save & Quit",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        );
      },
    );
  }

  void get() async{
    SharedPreferences sharedPreferences= await SharedPreferences.getInstance();
    String? name= await sharedPreferences.getString(key);
    if (name!=null){
      setState(() {
        username=name;
      });
    }
  }

  void add(str) async{
    SharedPreferences sharedPreferences= await SharedPreferences.getInstance();
    await sharedPreferences.setString(key, str);
    get();
  }
}
